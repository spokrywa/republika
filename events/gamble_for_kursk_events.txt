﻿namespace = gamble_for_kursk_events

gamble_for_kursk_events.1 = {
	type = country_event
	placement = root
	event_image = {
		video = "gfx/event_pictures/southamerica_public_figure_assassination.bk2"
	}
	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"
	on_opened_soundeffect = "event:/SFX/Events/southamerica/public_figure_assassination"
	icon = "gfx/interface/icons/event_icons/event_newspaper.dds"
	title = gamble_for_kursk_events.1.t
	desc = gamble_for_kursk_events.1.d
	flavor = gamble_for_kursk_events.1.f

	duration = 3

	trigger = {
		game_date >= 1836.1.1
		exists = c:RUS
		this = c:RUS
	}

	option = {
		name = gamble_for_kursk.1.a
		default_option = yes
        set_variable = {
			name = kursk_demanded
			value = yes
		}
		create_diplomatic_play = {
			name = gamble_for_kursk
			target_state = s:STATE_KURSK.region_state:UKR
			war = no		
			type = dp_return_state
			add_war_goal = {
				holder = c:RUS
				type = return_state
				target_state = s:STATE_KURSK.region_state:UKR
			}
		}
		c:UKR = {
			trigger_event = {
				id = gamble_for_kursk_events.2
				days = 5
				popup = yes
			}
		}	
	}

	option = {
		name = gamble_for_kursk_events.1.b
		capital = {
			add_radicals = {
				value = very_large_radicals
				pop_type = officers
			}
			add_radicals = {
				value = very_large_radicals
				pop_type = soldiers
			}
			add_radicals = {
				value = very_large_radicals
				strata = middle
			}
		}
	}
}

gamble_for_kursk_events.2 = {
	type = country_event
	placement = root
	event_image = {
		video = "gfx/event_pictures/southamerica_public_figure_assassination.bk2"
	}
	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"
	on_opened_soundeffect = "event:/SFX/Events/southamerica/public_figure_assassination"
	icon = "gfx/interface/icons/event_icons/event_newspaper.dds"
	title = gamble_for_kursk_events.2.t
	desc = gamble_for_kursk_events.2.d
	flavor = gamble_for_kursk_events.2.f

	duration = 3

	trigger = {
		exists = c:UKR
		this = c:UKR
		any_diplomatic_play = {
			initiator_is = c:RUS
			target_is = c:UKR
		}
	}

	option = {
		name = gamble_for_kursk.2.a
		default_option = yes
       	any_diplomatic_play  = {
       		initiator_is = c:RUS
       		target_is = c:UKR
       		add_war_goal = {
				holder = c:UKR
				type = return_state
				target_state = s:STATE_ROSTOV.region_state:RUS
			}
       	}
       	add_modifier = {
       		name = ukraine_offensive_footing
       		months = 12
       	}
		
	}

	option = {
		name = gamble_for_kursk_events.2.b
		add_modifier = {
       		name = ukraine_defensive_footing
       		months = 12
       	}
	}
}
