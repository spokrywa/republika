﻿kossak={
	color= hsv{ 0.53 0.24 0.38 }
	religion = orthodox
	traits = { east_slavic north_asian_heritage european_heritage }
	male_common_first_names={
		Alexei
		Andrei
		Anton
		Arkadiy
		Borys
		Dmitro
		Fyodor
		Hryhoriy
		Ivan
		Jangir
		Lavr
		Leonid
		Lev
		Mashur
		Murat
		Mikhail
		Mykola
		Nikita
		Oleksandr
		Panteleimon 
		Pavel
		Petro
		Roman
		Semyon
		Sergiy
		Songgotu
		Valeriy
		Viktor
		Vladislav
		Volodimir
		Yevhen
		Yuriy
		Zhao
		Iskander
		Makhmut
		Mullanur
		Musa
		Rizaeddin
		Rustam
		Xosayen
		Yadegar
		Yusuf
	}
	female_common_first_names = {
		Ablai
		Abu_al-Khayr
		Aisha
		Aryn
		Darya
		Dniprova
		Hrytsko
		Kolomba
		Lyubov
		Lyudmila
		Maria
		Nadazhda
		Nataliya
		Olena
		Olha
		Polina
		Ruslana
		Slava
		Ulyana
		Valeriya
		Xenia
		Yevheniya
	}	
	noble_last_names = {
		Chubynsky
		Czajkowski
		Drahomanov
		Gogol-Yanovsky
		Hnizdovsky
		Hrekov
		Hrushevsky
		Karaszewicz-Tokarzewski
		Kobyliansky
		Konysky
		Kropyvnytsky
		Kulish
		Levytsky
		Lototsky
		Ohonovsky
		Omelianovych-Pavlenko
		Ortynsky
		Petrushevych
		Rodzianko
		Sas-Kuilovsky
		Shakhovskoy
		Sheptytsky
		Skoropadskyi
		Vakhnianyn
		Vitovsky
		Zubrytsky
		Xian
		Xun
		Yi
		Ying
		Yu
		Zheng
		Zhi
		Zhon
		Zhuang
	}
	common_last_names={
		Bezruchko
		Chernyakhovsky
		Dyachenko
		Gamula
		Grigoriev
		Hrekov
		Ivanenko
		Ivanov
		Kapustiansky
		Khan
		Kondratenko
		Leshchenko
		Leshchinskiy
		Paskevich
		Petliura
		Radchenko
		Shevchenko
		Shoshak-ula
		Skoropadskyi
		Stefaniv
		Tymoshenko
		Vitovsky
		Amirkhanov
		Agumov
		Borhan
		Faysi
	}
	ethnicities = {
		1 = slavic
	}
	graphics = european
}
baltic_german={
color= rgb{ 14 35 187 }
	religion = protestant
	traits = { german_speaking baltic_culture_group european_heritage }
	male_common_first_names={
		Adelbert Adolf Albrecht Alexander Alfred August
		Bernhard Burkhard Bruno		
		Dieter
		Eduard Edwin Erich Ernst Erwin Esaias
		Florian Franz Friedrich Fritz
		Georg Gustav Gunther Giesebert
		Hasso Heinrich Helmuth Hermann Hugo Haubold Hillart
		Ignatz
		Joachim Johann Jurgen
		Karl Konrad
		Leonhard Leopold Ludwig Lukas
		Manfred Maximilian Markus Michael Moritz
		Nikolaus
		Otto Oskar
		Pascal Paul Peter Philipp
		Reinhard Rudolf Ruprecht
		Stefan
		Theodor
		Wilhelm Wolfgang
		Aaron
		Abrams
		Alberts
		Aleksandrs
		Aloisz
		Andrejs
		Arkadij
		Armands
		Arnolds
		Atis
		Brunis
		Bruno
		Eduards
		Eliass
		Elmars
		Emils
		Evgenijs
		Filips
		Fricis
		Gustavs
		Heinrihs
		Hermanis
		Izaks
		Janis
		Jazeps
		Jekabs
		Johans
		Juris
		Karlis
		Krisjanis
		Leo
		Mark
		Martins
		Mihails
		Mikelis
		Nikolajs
		Oskars
		Pauls
		Peteris
		Rihards
		Roberts
		Romans
		Sandis
		Sergejs
		Valdemars
		Valerians
		Valter
		Vilis
		Vladimirs
		Zigfrids
	}
	female_common_first_names = {
		Adelheid Armgard Adolphine Albertine Anna Amalie Anna_Dorothea Anna_Sophia Anna_Erika Aurora Agnes
		Beatrix
		Charlotte Cecilia Caroline Christina Clara
		Dorothea_Sophia Dorothea
		Elisabeth Erika
		Friederike 
		Hilda Henriette Hedwig
		Ingeborg
		Julia Johanna
		Luise
		Mathilde Margarethe Maria Marie_Elisabeth Magdalene
		Sigrid Sophie Sophie_Albertine
		Therese
		Viktoria
		Amida
		Antonija 
		Benigna
		Berta
		Diana
		Elfriede 
		Elza 
		Emilija
		Inguna
		Johanna
		Julija
		Karoline
		Klara
		Lidija
		Lizete
		Magda
		Marija
		Marta
		Matilde
		Regina
		Roze
	}
	noble_last_names = {
		von_Knabenau
		von_Lieven
		von_Mengden
		von_Plettenberg
		von_Stamm
		von_Wolff
	}	
	common_last_names={
		Abegg Anger Arendt Ackermann Arntz
		Behncke Boermel Borsig Brill Brommy Blum Baltzer Bever BA_hren Bischoff Brauweiler BrockmU_ller Broix BU_rgers BO_cking Biedermann BlO_de BO_hler Brockhaus Bauer Birnbaum Brunck Buff Bolten Briegleb Bardorf
		Clebsch Cordemann Culemeyer Caspers Cetto Clossett Colonius Compes Carriere Cretzschmar Cratz Cropp
		Dahlmann Dieffenbach Diestel Dreyer Droste Depre Dietze Detering Dietz DuprE_ Dresel
		Ehlert Eichelbaum Eckert Eisenstuck Eissengarthen Elwert Emmerling Ettlinger Erichson
		Feyerabend Fritsch Friedenthal Fabarius Forst Frings Freudentheil Felsing FlU_gge
		Garbe Gerlach Gloger Goerdeler Goetz Grebe Groener Groth Gerlich Giersberg Glaubrech Gail GU_lich
		Hagen Harkort Harych Hassenpflug Heppendorf Hirschfeld Hoffmann Hubatsch Hammer Hecker Herrmann HO_nninghaus Haustein Hensel Henkel Hildebrand Hallwachs Heldmann Hestermann Hesse Hoffmann HU_gel Hollandt
		Jastrow Johow Junghans Jahn Jordan Jungbluth Jungblutte Jucho
		Katz Kempner Kirchweger Koffka Krohn Krummnow Knocke Kehl Keiffenheim Kullich KU_hne Kahlert Kinscherf Kloch Kierulff Kriegk Kugler
		Lasker Leistikow Leonhardt Ludendorff Lehmkuhl Laist Lamberts Landfermann Leue Lingmann Lintz Lederer Lehne Lotheissen Leisler Lang
		Mittwoch Moll Mallmann Marcks Maurer MU_lhens Minckwitz Mohr Manecke Marktscheffel Mappes
		NettstrA_tter Neuerburg Neunzig Nohl
		Olbers Oetker
		Pelz Pax Peiser Plange Pagenstecher Peters Pfeiffer Philippi Prinzen Proff PflU_ger Pitschaft PrA_torius Preusser Pogge Pohle
		Rehfisch Roepke RU_ckert Ronge Reichensperger Reinartz Rittinghausen Ritz Rewitzer Rauschenplat RO_ssler RU_hl Ramspeck Rauschers Reh Runge Reinganum
		Scherfke Schimmelfennig Schmidt Schree Schurz Schnitter SchlO_ffel Stahlschmidt Schwetschke Schnake Schneider Schuchart SchU_renberg Schigard Scheidt Scherer Schleicher SchlO_sser Schmitz SchO_ller Stapper Stedmann Strom Schaffrath SchlU_ter Schnabel Schwarzenberg Steuernagel Stoll Strecker Schleiden Schlettwein Schnelle Stever Sonnenkalb Souchay
		Treibe TU_rke Theyssen Thilmany Trombetta
		Umbscheiden
		Venedy Vogt Volhard Varrentrapp Vorwerck
		Weydemeyer Wittfeld Woehlert Wiesner Wislicenus Wachendorf Waldschmidt Weckbecker Welcker WerlE_ Wesendonck Weilhelmi Winneritz Wirtz Wolfermann Wigard Wuttke Wippermann Wentorp Wendhausen
		Zerrahn ZO_ller Zell Zimmermann ZachariA_ Zulauf Zais
		Abakovskis
		Apsenieks
		Balodis
		Baumanis
		Berkis
		Berzin
		Betins
		Braons
		Caks
		Cakste
		Caps
		Celmins
		Dalins
		Dambitis
		Darzins
		Desslers
		Eizenteins
		Goppers
		Halsmans
		Irbitis
		Kalnins
		Kalpaks
		Kovalevskis
		Krogzems
		Kronvalds
		Kucis
		Kuks
		Kviesis
		Lacis
		Leiko
		Loskis
		Maters
		Matisons
		Meierovics
		Mihelssons
		Milenbahs
		Millers
		Needra
		Ozolins
		Petrovs
		Plieksans
		Pumpurs
		Rothko
		Simanis
		Stucka
		Tumins
		Ulmanis
		Veidenbaums
		de_Kolongs
	}

	male_regal_first_names = {
		Adalbert
		Abrams
		Alberts
		Aleksandrs
		Aloisz
		Friedrich Friedrich_Wilhelm
		Georg
		Heinrich
		Joachim
		Oskar
		Sigismund
		Wilhelm Waldermar
	}
	female_regal_first_names = {
		Augusta
		Charlotte
		Elisabeth
		Luise
		Margarethe
		Sophie
		Victoria
	}

	graphics = european
	ethnicities = {
		1 = caucasian
	}
}